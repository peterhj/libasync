//use std::cell::{UnsafeCell};
use std::marker::{PhantomData};
use std::ops::{Deref};

pub trait AsyncContext: Sized {
  fn synchronize(&self);
}

pub trait Async<'a>: Sized {
  type Ctx: AsyncContext;
}

pub trait SyncGuard<'a> {}

pub trait SyncLoad<'a>: Async<'a> {
  type SyncData;
  type Guard: SyncGuard<'a>;

  fn sync_load(&'a mut self, other: &'a Self::SyncData, ctx: &Self::Ctx) -> Self::Guard;
}

pub trait SyncStore<'a>: Async<'a> {
  type SyncMutData;
  type Guard: SyncGuard<'a>;

  fn sync_store(&'a self, other: &'a mut Self::SyncMutData, ctx: &Self::Ctx) -> Self::Guard;
}

pub trait AsyncLoad<'a>: Async<'a> {
  type AsyncCell;

  fn async_load(&mut self, other: &Self::AsyncCell, ctx: &Self::Ctx);
}

pub trait AsyncStore<'a>: Async<'a> {
  type AsyncCell;

  fn async_store(&self, other: &mut Self::AsyncCell, ctx: &Self::Ctx);
}

pub trait AsyncSend<'a>: Async<'a> {
  type Mutable: Async<'a>;

  fn send(&'a self, other: &'a mut Self::Mutable, ctx: &Self::Ctx);
}
